<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'Elitsa Vrancheva',
            'email' => 'elitsa.vr@gmail.com',
            'password' => bcrypt('1234'),
            'api_token' => str_random(60),
        ]);
    }
}
