<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comment;

class Post extends Model
{
	protected $fillable = ['title', 'body', 'user_id', 'created_at','updated_at'];
    public function user(){
    	return $this->belongsTo(User::class);
    }
     public function comments() {
        return $this->hasMany(Comment::class);
    }
}
