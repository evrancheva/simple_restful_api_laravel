<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
       if( isset($_GET['orderBy']) && $_GET['orderBy'] == "dateAsc"){
              $posts = Post::orderBy('created_at')->get();

        }
        elseif(isset($_GET['orderBy']) && $_GET['orderBy'] == "dateDesc"){

             $posts = Post::orderByDesc('created_at')->get();

        }
        elseif( isset($_GET['orderBy']) && $_GET['orderBy'] == "mostComments"){   
           
            $posts = Post::all()->sortByDesc(function ($item) { 
                    $item->comments()->count(); 
            }); 
         }
        else{
             $posts = Post::all();
        }      
      
       return response()->changeableFormat($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $title = $request->input('title');
        $body = $request->input('body');
        $user_id = Auth::guard('api')->id();
       return Post::create([
                'title' => $title,
                'body' => $body,
                'user_id' => $user_id,
                'created_at' => time(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sortPostsByDate(){
        $posts = Post::all()->orderBy('created_at', 'desc');
        return response()->changeableFormat($posts);

    }
}
