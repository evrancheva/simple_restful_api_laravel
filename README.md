# Quick summary #

The purpose of this application is to solve the following task:
Write a simple API for creating a new user, logging in a user, creating a post, writing a comment to a post, showing list of posts (sortable by creation date, and most commented posts). Creating a post and commenting on a post requires authenticated user. The response from all methods must be in JSON but the goal is to be able to swap out the response format without having to change any of the code that sends response. The code have to be written in object-oriented style. 


### Used Framework ###

Laravel 5.4

